// Funcion para generar numeros aleatorios
function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}
// creacion de la primera funcion
function funcion1(){
    //declaracion de un arreglo de tamaño 20 y variable para calcular el promedio y la suma
    let resultado = document.getElementById('funcion1');
    var arreglo1 = new Array(20);
    var suma = 0, promedio;
    //en caso de presionar el boton por segunda vez se limpiara el texto del resultado anterior
    resultado.innerHTML = ("");
    // Se le asignan valores al arreglo
    for (let i = 0; i < arreglo1.length; i++) {
        arreglo1[i] = getRandomInt(20);  
    }
    //comprobacion de los numeros del arreglo generados de manera aleatoria
    for (let i = 0; i < arreglo1.length; i++) {
        console.log(arreglo1[i]); 
    }
    // sumar la cantidad de numeros para posteriormente calcular el promedio
    for (let i = 0; i < arreglo1.length; i++) {
        suma = arreglo1[i] + suma;
    }
    //promedio de los numeros del primer arreglo
    promedio = suma / arreglo1.length;

    //mostrar el resultado
    resultado.innerHTML = resultado.innerHTML + "El promedio del arreglo [ " + arreglo1 + " ] es: " + promedio;
}
// creacion de la segunda funcion
function funcion2(){
    //declaracion de un arreglo de tamaño 20 y variable para calcular el promedio y la suma
    let resultado2 = document.getElementById('funcion2');
    var arreglo2 = new Array(20);
    var pares = 0;
    //en caso de presionar el boton por segunda vez se limpiara el texto del resultado anterior
    resultado2.innerHTML = ("");
     // Se le asignan valores al arreglo
     for (let i = 0; i < arreglo2.length; i++) {
        arreglo2[i] = getRandomInt(20);  
    }
    //comprobacion de los numeros del arreglo generados de manera aleatoria
    for (let i = 0; i < arreglo2.length; i++) {
        console.log(arreglo2[i]); 
    }
    // sumar la cantidad de numeros pares para posteriormente mostrarlo al usuario
    for (let i = 0; i < arreglo2.length; i++) {
        if ((arreglo2[i] % 2) == 0) {
            pares = pares + 1;
        }  
    }
    //mostrar el resultado
    resultado2.innerHTML = resultado2.innerHTML + "La cantidad de numeros pares en el arreglo [ " + arreglo2 + " ] es: " + pares;
}

function funcion3(){
    //declaracion de un arreglo de tamaño 20 y variable para calcular el promedio y la suma
    let resultado3 = document.getElementById('funcion3');
    var arreglo3 = new Array(20);
    var arregloD = new Array(20);
    var pares = 0;
    //en caso de presionar el boton por segunda vez se limpiara el texto del resultado anterior
    resultado3.innerHTML = ("");
     // Se le asignan valores al arreglo
     for (let i = 0; i < arreglo3.length; i++) {
        arreglo3[i] = getRandomInt(20);  
        arregloD[i] = arreglo3[i];
    }
    //comprobacion de los numeros del arreglo generados de manera aleatoria
    for (let i = 0; i < arreglo3.length; i++) {
        console.log("arreglo Original: "+arreglo3[i]); 
        console.log("arreglo Copia: "+arregloD[i]); 
    }
    //funcion incluida en JS que ayuda a ordenar valores numericos del mayor al menor, para hacer lo contrario solo se necesita intercambiar los valores en la segunda parte de la funcion
    arreglo3.sort(function(a,b){return b - a});

    //mostrar el resultado
    resultado3.innerHTML = resultado3.innerHTML + "La forma ordenada de mayor a menor del arreglo [ " + arregloD + " ] es: [" + arreglo3 +"]";
}